import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'django-test-site.settings'

sys.path.append('/path/to/django-test-site/')

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

# vim: set ft=python:

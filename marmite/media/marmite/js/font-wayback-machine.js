$(document).ready(function(){
    var commits;  // Commit Array
    var slider = $('input[type="range"]');
    var sha_elt = $('#sha');
    var msg_elt = $('#msg');
    var author_elt = $('#author');
    var date_elt = $('#date');

    $("#increase").click(function(event){
        event.preventDefault();
        var old_size = parseFloat($("figure").css("font-size"));
        $("figure").css("font-size", old_size + 10);
    });
    $("#decrease").click(function(event){
        event.preventDefault();
        var old_size = parseFloat($("figure").css("font-size"));
        $("figure").css("font-size", old_size - 10);
    });


    $.getJSON('commits/', function(data) {
        commits = data;
        slider.attr('min', 0).attr('max', commits.length);
    });

    slider.change(function() {
        //msg_elt.attr('value', commits[$(this).attr('value')]);

        $.getJSON("font/", { commit: commits[$(this).attr('value')] },
            function(data) {
                var styles = ''
                        + '@font-face {'
                        + '    font-family: "My Font";'
                        + '    src: url("data:font/opentype;base64,' + data['data']+ '");'
                        + '}'
                        + 'div.myfont {'
                        + '    font-family: "My Font";'
                        + '}';
                $('style[title="dynamic"]').html(styles);
                sha_elt.html(data['sha']);
                msg_elt.html(data['msg']);
                author_elt.html(data['author']);
                date_elt.html(data['date']);
            });
    });
});


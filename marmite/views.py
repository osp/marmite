## This file is part of Marmite
## Copyright (C) 2011 Open Source Publishing
## See AUTHORS for the full list of contributors
##
## Marmite is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with Marmite.  If not, see <http://www.gnu.org/licenses/>.


from base64 import b64encode
from git import Repo
try:
    import simplejson as json
except:
    import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


repo = Repo("/home/aleray/src/open-baskerville");


@csrf_exempt
def commits(request):
    return HttpResponse(json.dumps([i.sha for i in repo.iter_commits()]))

@csrf_exempt
def font(request):
    revision = request.GET.get("commit", "HEAD")
    commit = repo.commit(revision)
    tree = repo.tree(revision)
    return HttpResponse(json.dumps({
            'sha' : commit.sha[:7],
            'author' : commit.author.name,
            'date' : commit.committed_date,
            'msg' : commit.message,
            'data' : b64encode(tree["openbaskerville.ttf"].data),
        }))

@csrf_exempt
def all_revisions(request):
    commits = []
    for commit in repo.iter_commits():
        commits.append({
            'sha' : commit.sha,
            'msg' : commit.message,
            'data' : b64encode(commit.tree["openbaskerville.ttf"].data),
        })
    commits.reverse()  # Gets old commits first

    return HttpResponse(json.dumps(commits))

## This file is part of Marmite
## Copyright (C) 2011 Open Source Publishing
## See AUTHORS for the full list of contributors
##
## Marmite is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with Marmite.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls.defaults import *

urlpatterns = patterns('django.views.generic.simple',
    url(r'^$', 'direct_to_template', {'template': 'marmite/home.html'}, name='marmite-home'),
)

urlpatterns += patterns('',
    url(r'^font/$', 'marmite.views.font', {}, name='marmite-font'),
    url(r'^commits/$', 'marmite.views.commits', {}, name='marmite-font'),
    url(r'^all_revisions/$', 'marmite.views.all_revisions', {}, name='marmite-all-revisions'),
)
